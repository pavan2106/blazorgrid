﻿using BlazorApp1.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp1
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Weather> Get()
        {
            List<Weather> listWeathers = new List<Weather>();
            for (int i = 0; i < 50; i++)
            {
                listWeathers.Add(new Weather() { date = "2018-05-06", temperatureC = 1, summary = "" + i });
            }
            
            return listWeathers;
        }
//        public IEnumerable<Weather> Get()
//        {
//            return [
//  {
//                date: "2018-05-06",
//    temperatureC: 1,
//    summary: "Freezing"
//  },
//  {
//                date: "2018-05-07",
//    temperatureC: 14,
//    summary: "Bracing"
//  },
//  {
//                date: "2018-05-08",
//    temperatureC: -13,
//    summary: "Freezing"
//  },
//  {
//                date: "2018-05-09",
//    temperatureC: -16,
//    summary: "Balmy"
//  },
//  {
//                date: "2018-05-10",
//    temperatureC: -2,
//    summary: "Chilly"
//  },
//  {
//                date: "2018-05-06",
//    temperatureC: 1,
//    summary: "Freezing"
//  },
//  {
//                date: "2018-05-07",
//    temperatureC: 14,
//    summary: "Bracing"
//  },
//  {
//                date: "2018-05-08",
//    temperatureC: -13,
//    summary: "Freezing"
//  },
//  {
//                date: "2018-05-09",
//    temperatureC: -16,
//    summary: "Balmy"
//  },
//  {
//                date: "2018-05-10",
//    temperatureC: -2,
//    summary: "Chilly"
//  },
//  {
//                date: "2018-05-06",
//    temperatureC: 1,
//    summary: "Freezing"
//  },
//  {
//                date: "2018-05-07",
//    temperatureC: 14,
//    summary: "Bracing"
//  },
//  {
//                date: "2018-05-08",
//    temperatureC: -13,
//    summary: "Freezing"
//  },
//  {
//                date: "2018-05-09",
//    temperatureC: -16,
//    summary: "Balmy"
//  },
//  {
//                date: "2018-05-10",
//    temperatureC: -2,
//    summary: "Chilly"
//  },
//  {
//                date: "2018-05-06",
//    temperatureC: 1,
//    summary: "Freezing"
//  },
//  {
//                date: "2018-05-07",
//    temperatureC: 14,
//    summary: "Bracing"
//  },
//  {
//                date: "2018-05-08",
//    temperatureC: -13,
//    summary: "Freezing"
//  },
//  {
//                date: "2018-05-09",
//    temperatureC: -16,
//    summary: "Balmy"
//  },
//  {
//                date: "2018-05-10",
//    temperatureC: -2,
//    summary: "Chilly"
//  }
//]

        //        }
    }
}
