﻿namespace BlazorApp1.Models
{
    public class Weather
    {
        public string date { get; set; }

        public int temperatureC { get; set; }

        public string summary { get; set; }

    }
}
